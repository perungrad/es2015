const sum = (param1, param2) => {
    return param1 + param2;
};

const pow = (param) => {
    return param * param;
};

const pow2 = param => param * param;

const fero = {
    name: 'fero',
    tags: [ 'php', 'js', 'perl'],
    showScoreLog1: function () {
        this.tags.map(function (tag) {
            console.log(this.name + ' pozná ' + tag);
        });
    },
    showScoreLog2: function () {
        const that = this;

        this.tags.map(function (tag) {
            console.log(that.name + ' pozná ' + tag);
        });
    },
    showScoreLog3: function () {
        this.tags.map(tag => {
            console.log(this.name + ' pozná ' + tag);
        });
    }
};

//fero.showScoreLog1();
fero.showScoreLog2();
fero.showScoreLog3();
