import 'babel-polyfill';

let person = {id: 42};

person = Object.assign(
    person,
    {name: 'fero'},
    {name: 'jozo', surname: 'mrkvicka'},
    {name: 'ferko'}
);

console.log(person);
