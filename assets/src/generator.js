import 'babel-polyfill';

const gen = function* () {
    let n = 1;

    while (true) {
        yield n;
        n++;
    }
}

const generator = gen();

console.log(generator);

const val = generator.next();

console.log(val);
