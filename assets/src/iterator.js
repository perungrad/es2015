import 'babel-polyfill';

const counter = {
    [Symbol.iterator]: function* () {
        let n = 1;

        while (true) {
            yield n++;
        }
    }
};

for (const i of counter) {
    console.log(i);

    if (i >= 10) {
        break;
    }
}
