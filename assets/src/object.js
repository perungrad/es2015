const name = 'fero';

const fero = {
    name,

    foo() {
        // do something
    }
};

const fero2 = {
    name: name,
    foo: function () {
        // do something
    }
};
