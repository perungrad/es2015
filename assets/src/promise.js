import 'babel-polyfill';

function timeout(duration = 0) {
    return new Promise((resolve, reject) => {
        console.log('promise');
        setTimeout(resolve, duration);
    })
}

var p = timeout(1000).then(() => {
    console.log('then');
    return timeout(2000);
}).then(() => {
    console.log('hmm');
});

